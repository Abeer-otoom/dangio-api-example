<?php

namespace App\Api\Controllers;

use App\Api\Transformers\LessonTransformer;
use App\Models\Lesson;

class LessonsController extends BaseController {

    public function index() {
        $lesson = Lesson::all();
        return $this->collection($lesson, new LessonTransformer());
    }

}
